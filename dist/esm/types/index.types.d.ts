import { ButtonProps } from './components/Button.types';
import { InputProps } from './components/Input.types';
export { ButtonProps, InputProps };
